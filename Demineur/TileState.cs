﻿namespace Demineur
{
    [Flags]
    public enum TileState
    {
        Empty = 0,
        Mined = 1,
        Visited = 2,
        Flaged = 4,
        Unknowed = 8,
    }
}
