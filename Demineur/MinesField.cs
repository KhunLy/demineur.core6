﻿namespace Demineur
{
    public class MinesField
    {
        private readonly Random _rand;
        private readonly TileState[,] _tiles;
        public int NbBombs { get; }
        public int NbCols { get; }
        public int NbRows { get; }
        public int Visited { get; private set; }
        public DateTime? Start { get; private set; }
        public DateTime? End { get; private set; }

        public event Action<MinesField, int, int>? TileChanged;
        public event Action<MinesField, bool, double>? GameEnd;

        public MinesField(int nbCols, int nbRows, int nbBombs)
        {
            _rand = new Random();
            NbCols = nbCols;
            NbRows = nbRows;
            NbBombs = nbBombs;
            _tiles = new TileState[nbCols,nbRows];
            AddBombs();
        }

        public TileState GetState(int x, int y)
        {
            return _tiles[x, y];
        }

        public int GetValue(int x, int y)
        {
            if ((_tiles[x, y].HasFlag(TileState.Flaged) || _tiles[x, y].HasFlag(TileState.Unknowed)) && !_tiles[x, y].HasFlag(TileState.Visited) || _tiles[x, y].HasFlag(TileState.Mined))
            {
                return 0;
            }
            int sum = 0;
            Convoluate(x, y, (dx, dy) =>
            {
                if (_tiles[dx, dy].HasFlag(TileState.Mined))
                {
                    sum++;
                }
            });
            return sum;
        }

        public void CheckTile(int x, int y)
        {
            if (_tiles[x, y].HasFlag(TileState.Visited) || _tiles[x, y].HasFlag(TileState.Flaged))
            {
                return;
            }
            Start ??= DateTime.Now;
            _tiles[x, y] |= TileState.Visited;
            Visited++;
            if (_tiles[x, y].HasFlag(TileState.Mined))
            {
                TileChanged?.Invoke(this, x, y);
                GameEnd?.Invoke(this, false, 0); ;
                return;
            }
            int value = GetValue(x, y);
            if (value == 0)
            {
                Convoluate(x, y, (dx, dy) =>
                {
                    CheckTile(dx, dy);
                });
            }
            TileChanged?.Invoke(this, x, y);
            if(Visited + NbBombs == NbCols * NbRows)
            {
                End = DateTime.Now;
                GameEnd?.Invoke(this, true, CalculateDuration());
            }
        }

        public void ChangeState(int x, int y)
        {
            if (!_tiles[x, y].HasFlag(TileState.Visited))
            {
                if (_tiles[x, y] == TileState.Empty || _tiles[x, y] == TileState.Mined)
                {
                    _tiles[x, y] |= TileState.Flaged;
                }
                else if (_tiles[x, y].HasFlag(TileState.Flaged))
                {
                    _tiles[x, y] &= ~TileState.Flaged;
                    _tiles[x, y] |= TileState.Unknowed;
                }
                else if (_tiles[x, y].HasFlag(TileState.Unknowed))
                {
                    _tiles[x, y] &= ~TileState.Unknowed;
                }
                TileChanged?.Invoke(this, x, y);
            }
        }

        private void Convoluate(int x, int y, Action<int, int> action)
        {
            for (int j = -1; j < 2; j++)
            {
                for (int i = -1; i < 2; i++)
                {
                    int dx = x + i;
                    int dy = y + j;
                    if ((i != 0 || j != 0) && dx >= 0 && dy >= 0 && dx < NbCols && dy < NbRows)
                    {
                        action(dx, dy);
                    }
                }
            }
        }

        private void AddBombs()
        {   
            int added = 0;
            while (added < NbBombs)
            {
                int x = _rand.Next(0, NbCols);
                int y = _rand.Next(0, NbRows);
                if (!_tiles[x, y].HasFlag(TileState.Mined))
                {
                    _tiles[x, y] |= TileState.Mined;
                    added++;
                }
            }
        }

        private double CalculateDuration()
        {
            return End.GetValueOrDefault().Subtract(Start.GetValueOrDefault()).TotalSeconds;
        }
    }
}
