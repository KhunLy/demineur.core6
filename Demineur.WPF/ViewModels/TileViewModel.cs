﻿using Prism.Mvvm;

namespace Demineur.WPF.Models
{
    internal class TileViewModel : BindableBase
    {
        private TileState _state;
        private int _value;
        public TileViewModel(TileState state, int value)
        {
            State = state;
            Value = value;
        }

        public TileState State
        {
            get
            {
                return _state;
            }

            set
            {
                SetProperty(ref _state, value);
            }
        }

        public int Value
        {
            get
            {
                return _value;
            }

            set
            {
                SetProperty(ref _value, value);
            }
        }
    }
}
