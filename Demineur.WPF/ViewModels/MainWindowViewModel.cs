﻿using Demineur.WPF.Models;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System.Collections.ObjectModel;
using System.Linq;

namespace Demineur.WPF.ViewModels
{
    internal class MainWindowViewModel : BindableBase
    {
        private readonly IDialogService _dialogService;

        private int _rows = 10;
        private int _columns = 10;
        private int _bombs = 10;
        private bool _inGame = true;
        private DelegateCommand? _startCommand;
        private DelegateCommand<TileViewModel>? _checkCommand;
        private DelegateCommand<TileViewModel>? _changeStateCommand;
        private ObservableCollection<ObservableCollection<TileViewModel>> _tiles;
        private MinesField _field;

        public MainWindowViewModel(IDialogService dialogService)
        {
            _dialogService = dialogService;
            _tiles = new ObservableCollection<ObservableCollection<TileViewModel>>();
            _field = new MinesField(Columns, Rows, Bombs);
            FillCollection();
        }

        public int Rows
        {
            get => _rows;
            set
            {
                SetProperty(ref _rows, value);
                StartCommand.RaiseCanExecuteChanged();
            }
        }

        public int Columns
        {
            get => _columns;
            set
            {
                SetProperty(ref _columns, value);
                StartCommand.RaiseCanExecuteChanged();
            }
        }

        public int Bombs
        {
            get => _bombs;
            set
            {
                SetProperty(ref _bombs, value);
                StartCommand.RaiseCanExecuteChanged();
            }
        }

        public bool InGame
        {
            get => _inGame;
            set
            {
                _inGame = value;
                _checkCommand?.RaiseCanExecuteChanged();
                _changeStateCommand?.RaiseCanExecuteChanged();
            }
        }

        public DelegateCommand StartCommand { get => _startCommand ??= new DelegateCommand(Start, CanStart); }
        public DelegateCommand<TileViewModel> CheckCommand { get => _checkCommand ??= new DelegateCommand<TileViewModel>(Check, CanPlay); }
        public DelegateCommand<TileViewModel> ChangeStateCommand { get => _changeStateCommand ??= new DelegateCommand<TileViewModel>(ChangeState, CanPlay); }
        public ObservableCollection<ObservableCollection<TileViewModel>> Tiles { get => _tiles; }

        

        private bool CanStart()
        {
            return _bombs != 0 && _columns != 0 && _rows != 0 && _bombs < _columns * _rows;
        }

        private void Start()
        {
            _field = new MinesField(Columns, Rows, Bombs);
            FillCollection();
            InGame = true;
        }

        private void Check(TileViewModel tile)
        {
            int y = _tiles.IndexOf(_tiles.First(l => l.Contains(tile)));
            int x = _tiles[y].IndexOf(tile);
            _field.CheckTile(x, y);
        }

        private void ChangeState(TileViewModel tile)
        {
            int y = _tiles.IndexOf(_tiles.First(l => l.Contains(tile)));
            int x = _tiles[y].IndexOf(tile);
            _field.ChangeState(x, y);
        }

        private bool CanPlay(TileViewModel tile)
        {

            return ((!tile?.State.HasFlag(TileState.Visited)) ?? true) && _inGame;
        }

        private void FillCollection()
        {
            _tiles.Clear();
            for (int y = 0; y < Rows; y++)
            {
                _tiles.Add(new ObservableCollection<TileViewModel>());
                for (int x = 0; x < Columns; x++)
                {
                    _tiles[y].Add(new TileViewModel(_field.GetState(x, y), _field.GetValue(x, y)));
                }
            }
            _field.TileChanged += TileChangedHandler;
            _field.GameEnd += GameEndHandler;
        }

        private void GameEndHandler(MinesField field, bool won, double duration)
        {
            InGame = false;
            if(won)
            {
                _dialogService.Show("NotificationDialog", new DialogParameters($"message={duration}"), null);
            }
            else
            {
                _dialogService.Show("NotificationDialog", new DialogParameters($"message=Vous avez perdu"), null);
            }
        }

        private void TileChangedHandler(MinesField field, int x, int y)
        {
            _tiles[y][x].State = field.GetState(x, y);
            _tiles[y][x].Value = field.GetValue(x, y);
            RaisePropertyChanged(nameof(Tiles));
        }
    }
}
