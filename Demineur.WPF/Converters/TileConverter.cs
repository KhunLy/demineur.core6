﻿using Demineur.WPF.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Demineur.WPF.Converters
{
    internal class TileConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length == 2 && values[0] is int value && values[1] is TileState state)
            {
                if (state.HasFlag(TileState.Visited))
                {
                    if (state.HasFlag(TileState.Mined))
                    {
                        return "💣";
                    }
                    else
                    {
                        return value == 0 ? "" : value;
                    }
                }
                else
                {
                    if (state.HasFlag(TileState.Flaged))
                    {
                        return "🚩";
                    }
                    if (state.HasFlag(TileState.Unknowed))
                    {
                        return "?";
                    }
                }
            }
            else
            {
                throw new ArgumentException();
            }
            return "";
        }


        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
