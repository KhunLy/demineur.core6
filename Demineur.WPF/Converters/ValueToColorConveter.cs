﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Demineur.WPF.Converters
{
    internal class ValueToColorConveter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is int v)
            {
                switch(v)
                {
                    case 0: return new SolidColorBrush(Color.FromRgb(0,0,0));
                    case 1: return new SolidColorBrush(Color.FromRgb(0,0,255));
                    case 2: return new SolidColorBrush(Color.FromRgb(0,255,0));
                    case 3: return new SolidColorBrush(Color.FromRgb(255,0,0));
                    case 4: return new SolidColorBrush(Color.FromRgb(0,0,0));
                    case 5: return new SolidColorBrush(Color.FromRgb(0,0,0));
                    case 6: return new SolidColorBrush(Color.FromRgb(0,0,0));
                    case 7: return new SolidColorBrush(Color.FromRgb(0,0,0));
                    case 8: return new SolidColorBrush(Color.FromRgb(0,0,0));
                }
            }
            return new SolidColorBrush(Color.FromRgb(0, 0, 0));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
