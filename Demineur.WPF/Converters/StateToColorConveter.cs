﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace Demineur.WPF.Converters
{
    internal class StateToColorConveter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is TileState state)
            {
                if(state.HasFlag(TileState.Visited))
                {
                    if (state.HasFlag(TileState.Mined))
                    {
                        return new SolidColorBrush(Color.FromRgb(255,0,0));
                    }
                    else
                    {
                        return new SolidColorBrush(Color.FromRgb(255, 255, 240));
                    }
                }
                else
                {
                    return new SolidColorBrush(Color.FromRgb(220, 220, 220));
                }
            }
            return new SolidColorBrush(Color.FromRgb(255,255,255));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
