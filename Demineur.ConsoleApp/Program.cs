﻿using Demineur;

ConsoleUtils.Maximize();

Init();

void Init()
{
    Console.CursorVisible = true;
    int nbCols = ConsoleUtils.Question<int>("Nombres de colonnes ?", x => x != 0);
    int nbRows = ConsoleUtils.Question<int>("Nombres de lignes ?", x => x != 0);
    int nbBombs = ConsoleUtils.Question<int>("Nombres de bombes ?", x => x != 0 && x < nbRows * nbCols);
    MinesField field = new MinesField(nbCols, nbRows, nbBombs);
    field.TileChanged += DisplayTileHandler;
    field.GameEnd += GameEndHandler;
    
    DisplayEmpty(field);
    Console.CursorVisible = false;
    Play(field);
}

void Play(MinesField field)
{
    int x = 0;
    int y = 0;
    while (true)
    {
        // sélectionner la case en changeant l'arrière plan
        Move(x, y);
        Console.BackgroundColor = ConsoleColor.DarkGray;
        DisplayTile(field, x, y);

        ConsoleKey key = Console.ReadKey(true).Key;

        // désélectionner la case en remettant l'arrière plan avec la couleur d'origine
        Move(x, y);
        DisplayTile(field, x, y);

        SelectAction(key);
    }

    void SelectAction(ConsoleKey key)
    {
        switch (key)
        {
            case ConsoleKey.UpArrow:
                if (y > 0)
                    y--;
                break;
            case ConsoleKey.DownArrow:
                if (y + 1 < field.NbRows)
                    y++;
                break;
            case ConsoleKey.LeftArrow:
                if (x > 0)
                    x--;
                break;
            case ConsoleKey.RightArrow:
                if (x + 1 < field.NbCols)
                    x++;
                break;
            case ConsoleKey.Spacebar:
                field.CheckTile(x, y);
                break;
            case ConsoleKey.C:
                field.ChangeState(x, y);
                break;
        }
    }
}


void DisplayEmpty(MinesField field)
{
    Console.Clear();
    Console.WriteLine(CreateLine('┌', '┐', '┬', CreateList("───", field.NbCols)));
    for (int i = 0; i < field.NbRows; i++)
    {
        Console.WriteLine(CreateLine('│', '│', '│', CreateList(" ■ ", field.NbCols)));
        if(i + 1 < field.NbRows)
        {
            Console.WriteLine(CreateLine('├', '┤', '┼', CreateList("───", field.NbCols)));
        }
    }
    Console.WriteLine(CreateLine('└', '┘', '┴', CreateList("───", field.NbCols)));

    Console.WriteLine("\nPress arrows to move, spacebar to check tile, c to change status");

    IEnumerable<string> CreateList(string v, int length)
    {
        for (int i = 0; i < length; i++)
        {
            yield return v;
        }
    }
    string CreateLine(char start, char end, char join, IEnumerable<string> items)
    {
        return start + string.Join(join, items) + end;
    }
}

void DisplayTileHandler(MinesField field, int x, int y)
{
    Move(x, y);
    DisplayTile(field, x, y);
}

void DisplayTile(MinesField field, int x, int y)
{
    TileState state = field.GetState(x, y);
    int value = field.GetValue(x, y);
    if (state.HasFlag(TileState.Visited))
    {
        if (state.HasFlag(TileState.Mined))
        {
            Console.BackgroundColor = ConsoleColor.DarkRed;
            ConsoleUtils.Write("Ø", ConsoleColor.Black);
        }
        else
        {
            WriteValue(value);
        }
    }
    else
    {
        if (state == TileState.Empty || state == TileState.Mined)
        {
            ConsoleUtils.Write("■", ConsoleColor.Gray);
        }
        if (state.HasFlag(TileState.Flaged))
        {
            ConsoleUtils.Write("¶", ConsoleColor.White);
        }
        if (state.HasFlag(TileState.Unknowed))
        {
            ConsoleUtils.Write("?", ConsoleColor.Yellow);
        }
    }
}

void WriteValue(int value)
{
    switch (value)
    {
        case 0:
            ConsoleUtils.Write(" ");
            break;
        case 1:
            ConsoleUtils.Write(value, ConsoleColor.Blue);
            break;
        case 2:
            ConsoleUtils.Write(value, ConsoleColor.Green);
            break;
        case 3:
            ConsoleUtils.Write(value, ConsoleColor.Red);
            break;
        case 4:
            ConsoleUtils.Write(value, ConsoleColor.DarkMagenta);
            break;
        case 5:
            ConsoleUtils.Write(value, ConsoleColor.Magenta);
            break;
        case 6:
            ConsoleUtils.Write(value, ConsoleColor.Cyan);
            break;
        case 7:
            ConsoleUtils.Write(value, ConsoleColor.DarkGreen);
            break;
        case 8:
            ConsoleUtils.Write(value, ConsoleColor.DarkCyan);
            break;
    }
}

void GameEndHandler(MinesField field, bool won, double duration)
{
    Console.SetCursorPosition(0, field.NbRows * 2 + 3);
    ConsoleUtils.Write(won ? "Gagné" : "Perdu", ConsoleColor.Yellow);
    ConsoleUtils.Write("\nDurée: ");
    ConsoleUtils.Write(duration, ConsoleColor.Yellow);
    Console.WriteLine("\nPress any key to continue");
    Console.ReadKey(true);
    Console.Clear();
    Init();
}

void Move(int x, int y)
{
    Console.SetCursorPosition((x * 4) + 2, (y * 2) + 1);
}