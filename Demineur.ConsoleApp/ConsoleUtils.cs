﻿using System.Runtime.InteropServices;

namespace Demineur
{
    static class ConsoleUtils
    {
        public static void Write(object text, ConsoleColor color = ConsoleColor.Gray)
        {
            Console.ForegroundColor = color;
            Console.Write(text);
            Console.ResetColor();
        }

        public static T Question<T>(string question, Func<T, bool>? isValid = null)
        {
            string result = Question(question);
            try
            {
                T value = (T)Convert.ChangeType(result, typeof(T));
                if(!(isValid?.Invoke(value) ?? true))
                {
                    throw new Exception();
                }
                return value;
            }
            catch (Exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Valeur incorrecte");
                return Question<T>(question, isValid);
            }
        }

        public static string Question(string question)
        {
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine(question);
            Console.ResetColor();
            return Console.ReadLine() ?? string.Empty;
        }

        public static void Maximize()
        {

            [DllImport("kernel32.dll", ExactSpelling = true)]
            static extern IntPtr GetConsoleWindow();
            [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
            static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
            ShowWindow(GetConsoleWindow(), 3);
        }
    }
}
